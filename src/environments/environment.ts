// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyArHOR4QcI8zk0_CzNVsvoI9rpiV8T2aI0",
    authDomain: "angular-kanban-ceed1.firebaseapp.com",
    projectId: "angular-kanban-ceed1",
    storageBucket: "angular-kanban-ceed1.appspot.com",
    messagingSenderId: "714294934289",
    appId: "1:714294934289:web:afb956a05bc9a594c42ab0",
    measurementId: "G-PTN588N4BS"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
